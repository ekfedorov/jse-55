package ru.ekfedorov.tm.exception.incorrect;

import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.exception.AbstractException;

public final class IncorrectArgumentException extends AbstractException {

    public IncorrectArgumentException(@NotNull final String arg) {
        super("Error! Argument ``" + arg + "`` not found...");
    }

}
