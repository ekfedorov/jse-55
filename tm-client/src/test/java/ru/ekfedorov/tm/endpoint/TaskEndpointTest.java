package ru.ekfedorov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.ekfedorov.tm.cofiguration.ClientConfiguration;
import ru.ekfedorov.tm.marker.IntegrationCategory;

import java.util.List;


public class TaskEndpointTest {

    @NotNull
    public final AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ClientConfiguration.class);

    @NotNull
    private final SessionEndpoint sessionEndpoint = context.getBean(SessionEndpoint.class);

    @NotNull
    private final TaskEndpoint taskEndpoint = context.getBean(TaskEndpoint.class);

    @NotNull
    private final ProjectEndpoint projectEndpoint = context.getBean(ProjectEndpoint.class);

    private Session session;

    private Session sessionAdmin;

    @Before
    @SneakyThrows
    public void before() {
        session = sessionEndpoint.openSession("test", "test");
        sessionAdmin = sessionEndpoint.openSession("admin", "admin");
        taskEndpoint.clearBySessionTask(session);
        taskEndpoint.clearBySessionTask(sessionAdmin);
        projectEndpoint.clearBySessionProject(session);
        projectEndpoint.clearBySessionProject(sessionAdmin);
    }

    @After
    @SneakyThrows
    public void after() {
        taskEndpoint.clearBySessionTask(session);
        taskEndpoint.clearBySessionTask(sessionAdmin);
        sessionEndpoint.closeSession(session);
        sessionEndpoint.closeSession(sessionAdmin);

    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void addTaskTest() {
        final String taskName = "nameTest";
        final String taskDescription = "nameTest";
        taskEndpoint.addTask(session, taskName, taskDescription);
        final Task task = taskEndpoint.findTaskOneByName(session, taskName);
        Assert.assertNotNull(task);
        Assert.assertEquals(taskName, task.getName());
        Assert.assertEquals(taskDescription, task.getDescription());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void bindTaskByProjectTest() {
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        taskEndpoint.bindTaskByProject(session, project.getId(), task.getId());
        final Task taskBind = taskEndpoint.findTaskOneById(session, task.getId());
        Assert.assertNotNull(taskBind);
        Assert.assertEquals(project.getId(), taskBind.getProjectId());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void changeTaskStatusByIdTest() {
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.changeTaskStatusById(session, task.getId(), Status.COMPLETE);
        final Task taskChanged = taskEndpoint.findTaskOneById(session, task.getId());
        Assert.assertEquals(Status.COMPLETE, taskChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void changeTaskStatusByIndexTest() {
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        final List<Task> tasks = taskEndpoint.findAllTask(session);

        int pos = 0;
        for(Task t : tasks) {
            if(task.getId().equals(t.getId())) break;
            pos++;
        }

        taskEndpoint.changeTaskStatusByIndex(session, pos, Status.COMPLETE);
        final Task taskChanged = taskEndpoint.findTaskOneById(session, task.getId());
        Assert.assertEquals(Status.COMPLETE, taskChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void changeTaskStatusByNameTest() {
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.changeTaskStatusByName(session, task.getName(), Status.COMPLETE);
        final Task taskChanged = taskEndpoint.findTaskOneById(session, task.getId());
        Assert.assertEquals(Status.COMPLETE, taskChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findAllByProjectIdTest() {
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        final Task task1 = taskEndpoint.addTask(session, "taskTest", "descrTest");
        final Task task2 = taskEndpoint.addTask(session, "taskTest2", "descrTest");
        final Task task3 = taskEndpoint.addTask(session, "taskTest3", "descrTest");
        taskEndpoint.bindTaskByProject(session, project.getId(), task1.getId());
        taskEndpoint.bindTaskByProject(session, project.getId(), task2.getId());
        taskEndpoint.bindTaskByProject(session, project.getId(), task3.getId());
        Assert.assertEquals(3, taskEndpoint.findAllByProjectId(session, project.getId()).size());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findAllTaskTest() {
        taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.addTask(session, "taskTest2", "descrTest");
        taskEndpoint.addTask(session, "taskTest3", "descrTest");
        Assert.assertEquals(3, taskEndpoint.findAllTask(session).size());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findTaskAllWithComparatorTest() {
        taskEndpoint.addTask(session, "ctaskTest", "descrTest");
        taskEndpoint.addTask(session, "ataskTest2", "descrTest");
        taskEndpoint.addTask(session, "btaskTest3", "descrTest");
        final List<Task> tasks = taskEndpoint.findTaskAllWithComparator(session, "NAME");
        Assert.assertEquals("ataskTest2", tasks.get(0).getName());
        Assert.assertEquals("btaskTest3", tasks.get(1).getName());
        Assert.assertEquals("ctaskTest", tasks.get(2).getName());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findTaskOneByIdTest() {
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        Assert.assertNotNull(taskEndpoint.findTaskOneById(session, task.getId()));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findTaskOneByIndexTest() {
        taskEndpoint.addTask(session, "taskTest0", "descrTest");
        taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.addTask(session, "taskTest3", "descrTest");
        final Task task = taskEndpoint.findAllTask(session).get(1);
        final Task taskFind = taskEndpoint.findTaskOneByIndex(session, 1);
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(task.getId(), taskFind.getId());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findTaskOneByNameTest() {
        taskEndpoint.addTask(session, "taskTest0", "descrTest");
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.addTask(session, "taskTest3", "descrTest");
        final Task taskFind = taskEndpoint.findTaskOneByName(session, "taskTest");
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(task.getId(), taskFind.getId());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void finishTaskByIdTest() {
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.finishTaskById(session, task.getId());
        final Task taskChanged = taskEndpoint.findTaskOneById(session, task.getId());
        Assert.assertEquals(Status.COMPLETE, taskChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void finishTaskByIndexTest() {
        taskEndpoint.addTask(session, "taskTest0", "descrTest");
        taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.addTask(session, "taskTest3", "descrTest");
        final Task task = taskEndpoint.findAllTask(session).get(1);
        taskEndpoint.finishTaskByIndex(session, 1);
        final Task taskChanged = taskEndpoint.findTaskOneById(session, task.getId());
        Assert.assertEquals(Status.COMPLETE, taskChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void finishTaskByNameTest() {
        taskEndpoint.addTask(session, "taskTest0", "descrTest");
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.addTask(session, "taskTest3", "descrTest");
        taskEndpoint.finishTaskByName(session, "taskTest");
        final Task taskChanged = taskEndpoint.findTaskOneByName(session, task.getName());
        Assert.assertEquals(Status.COMPLETE, taskChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeTaskTest() {
        taskEndpoint.addTask(session, "taskTest0", "descrTest");
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.addTask(session, "taskTest3", "descrTest");
        Assert.assertNotNull(taskEndpoint.findTaskOneById(session, task.getId()));
        taskEndpoint.removeTask(session, task);
        Assert.assertNull(taskEndpoint.findTaskOneById(session, task.getId()));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeTaskOneByIdTest() {
        taskEndpoint.addTask(session, "taskTest0", "descrTest");
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.addTask(session, "taskTest3", "descrTest");
        Assert.assertNotNull(taskEndpoint.findTaskOneById(session, task.getId()));
        taskEndpoint.removeTaskOneById(session, task.getId());
        Assert.assertNull(taskEndpoint.findTaskOneById(session, task.getId()));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeTaskOneByIndexTest() {
        taskEndpoint.addTask(session, "taskTest0", "descrTest");
        taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.addTask(session, "taskTest3", "descrTest");
        final Task task = taskEndpoint.findAllTask(session).get(1);
        Assert.assertNotNull(taskEndpoint.findTaskOneById(session, task.getId()));
        taskEndpoint.removeTaskOneByIndex(session, 1);
        Assert.assertNull(taskEndpoint.findTaskOneById(session, task.getId()));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeTaskOneByNameTest() {
        taskEndpoint.addTask(session, "taskTest0", "descrTest");
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.addTask(session, "taskTest3", "descrTest");
        Assert.assertNotNull(taskEndpoint.findTaskOneById(session, task.getId()));
        taskEndpoint.removeTaskOneByName(session, "taskTest");
        Assert.assertNull(taskEndpoint.findTaskOneById(session, task.getId()));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void startTaskByIdTest() {
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.startTaskById(session, task.getId());
        final Task taskChanged = taskEndpoint.findTaskOneById(session, task.getId());
        Assert.assertEquals(Status.IN_PROGRESS, taskChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void startTaskByIndexTest() {
        taskEndpoint.addTask(session, "taskTest0", "descrTest");
        taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.addTask(session, "taskTest3", "descrTest");
        final Task task = taskEndpoint.findAllTask(session).get(1);
        taskEndpoint.startTaskByIndex(session, 1);
        final Task taskChanged = taskEndpoint.findTaskOneById(session, task.getId());
        Assert.assertEquals(Status.IN_PROGRESS, taskChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void startTaskByNameTest() {
        taskEndpoint.addTask(session, "taskTest0", "descrTest");
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.addTask(session, "taskTest3", "descrTest");
        taskEndpoint.startTaskByName(session, "taskTest");
        final Task taskChanged = taskEndpoint.findTaskOneByName(session, task.getName());
        Assert.assertEquals(Status.IN_PROGRESS, taskChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void unbindTaskFromProjectTest() {
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        projectEndpoint.addProject(session, "projectTest", "descrTest");
        taskEndpoint.unbindTaskFromProject(session, task.getId());
        Assert.assertNull(taskEndpoint.findTaskOneById(session, task.getId()).getProjectId());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void updateTaskByIdTest() {
        final String newName = "taskTestNew";
        final String newDescription = "descrTestNew";
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.updateTaskById(session, task.getId(), newName, newDescription);
        final Task taskUpdate = taskEndpoint.findTaskOneById(session, task.getId());
        Assert.assertEquals(newName, taskUpdate.getName());
        Assert.assertEquals(newDescription, taskUpdate.getDescription());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void updateTaskByIndexTest() {
        final String newName = "taskTestNew";
        final String newDescription = "descrTestNew";
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.updateTaskByIndex(session, 0, newName, newDescription);
        final Task taskUpdate = taskEndpoint.findTaskOneById(session, task.getId());
        Assert.assertEquals(newName, taskUpdate.getName());
        Assert.assertEquals(newDescription, taskUpdate.getDescription());
    }

}

