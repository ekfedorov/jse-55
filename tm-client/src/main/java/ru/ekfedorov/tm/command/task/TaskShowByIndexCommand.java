package ru.ekfedorov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.command.AbstractTaskCommand;
import ru.ekfedorov.tm.endpoint.*;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.exception.system.NullTaskException;
import ru.ekfedorov.tm.util.TerminalUtil;

@Component
public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show task by index.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-view-by-index";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (sessionService == null) throw new NullObjectException();
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Task task = taskEndpoint.findTaskOneByIndex(session, index);
        if (task == null) throw new NullTaskException();
        showTask(task);
        System.out.println();
    }

}
