package ru.ekfedorov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.ekfedorov.tm.api.service.ICommandService;
import ru.ekfedorov.tm.bootstrap.Bootstrap;
import ru.ekfedorov.tm.service.SessionService;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public abstract class AbstractCommand {

    @NotNull
    @Autowired
    protected ICommandService commandService;

    @Nullable
    @Autowired
    protected SessionService sessionService;

    @Nullable
    public abstract String commandArg();

    @Nullable
    public abstract String commandDescription();

    @Nullable
    public abstract String commandName();

    public abstract void execute();

    @NotNull
    @Override
    public String toString() {
        String result = "";
        if (!isEmpty(commandName())) result += commandName();
        if (!isEmpty(commandArg())) result += " [" + commandArg() + "]";
        if (!isEmpty(commandDescription())) result += " - " + commandDescription();
        return result;
    }

}
