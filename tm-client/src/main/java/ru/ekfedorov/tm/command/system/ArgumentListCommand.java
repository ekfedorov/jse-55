package ru.ekfedorov.tm.command.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.command.AbstractCommand;

import java.util.Collection;

@Component
public final class ArgumentListCommand extends AbstractCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show program arguments.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "arguments";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final Collection<String> arguments = commandService.getListArgumentName();
        System.out.println("[ARGUMENTS]");
        for (@Nullable final String argument : arguments) {
            if (argument != null) System.out.println(argument);
        }
    }

}
